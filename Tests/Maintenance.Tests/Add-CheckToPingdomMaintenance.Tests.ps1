Import-Module '.\Pingdom.Management.Maintenance.psm1' -Force

Describe 'Add-CheckToPingdomMaintenance' {  
  Context 'Add Check To Maintenance Item Using Default Options' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Maintenance { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $MaintenanceID = '55555'
      $CheckID = '66666'
      Add-CheckToPingdomMaintenance -AppKey $AppKey -MaintenanceID $MaintenanceID -CheckID $CheckID
    }

    It 'Uses Put Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Method -eq 'Put'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        
        # The function calls another function which also calls Invoke Rest Method with the same path.  Add Method filter make ensure testing of correct call.
        $Method -eq 'Put' -and $URI -like "*/api/*/maintenance/$MaintenanceID"
      }
    }

    It 'Contains Required Body Attributes' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        ($Body = @{
          uptimeids = $CheckID
        } | ConvertTo-Json -Compress) -eq $Body -and $Method -eq 'Put'
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        # The function calls another function which also calls Invoke Rest Method with the same path.  Add Method filter make ensure testing of correct call.
        $Method -eq 'Put' -and $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }

  Context 'Add Check To Maintenance Items Using Optional Parameters' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Maintenance { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $APIVersion = '99.9'
      $PingdomURL = 'https://api2.pingdom.com'
      $AppKey = '1234'
      $MaintenanceID = '55555'
      $CheckID = '66666'

      $Params = @{
        AppKey        = $AppKey
        APIVersion    = $APIVersion
        MaintenanceID = $MaintenanceID
        CheckID       = $CheckID
        PingdomURL    = $PingdomURL 
      }
      Add-CheckToPingdomMaintenance @Params
    }





    It 'Uses Post Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Method -eq 'Put'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $URI -eq "$PingdomURL/api/$APIVersion/maintenance/$MaintenanceID"
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        # The function calls another function which also calls Invoke Rest Method with the same path.  Add Method filter make ensure testing of correct call.
        $Method -eq 'Put' -and $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }
  Context 'Add Multiple Checks To Maintenance Item VIA Pipeline ' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Maintenance { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $MaintenanceID = '55555'
      $CheckIDs = @('66666', '77777', '88888')
      $CheckIDs | Add-CheckToPingdomMaintenance -AppKey $AppKey -MaintenanceID $MaintenanceID
    }
    It 'Uses Put Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Method -eq 'Put'
      }
    }
  
    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        
        # The function calls another function which also calls Invoke Rest Method with the same path.  Add Method filter make ensure testing of correct call.
        $Method -eq 'Put' -and $URI -like "*/api/*/maintenance/$MaintenanceID"
      }
    }
  
    It 'Contains Required Body Attributes' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        ($Body = @{
          uptimeids = $CheckIDs -join (',')
        } | ConvertTo-Json -Compress) -eq $Body -and $Method -eq 'Put'
      }
    }
  
    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        # The function calls another function which also calls Invoke Rest Method with the same path.  Add Method filter make ensure testing of correct call.
        $Method -eq 'Put' -and $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }
}


