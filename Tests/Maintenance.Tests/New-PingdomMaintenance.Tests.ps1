Import-Module '.\Pingdom.Management.Maintenance.psm1' -Force

Describe 'New-PingdomMaintenance' {
  Context 'Create New Maintenance Item Using Default Options' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Maintenance { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $Description = 'MyTestMaintenance'
      $StartUTC = '01/01/2000 10:00 am.'
      $EndUTC = '01/01/2000 11:00 am'
      New-PingdomMaintenance -AppKey $AppKey -Description $Description -StartUTC $StartUTC -EndUTC $EndUTC
    }


    It 'Uses Post Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Method -eq 'Post'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $URI -like "*/api/*/maintenance"
      }
    }

    It 'Contains Required Body Attributes' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Body -like "*$StartEpoc*" -and $Body -like "*$EndEpoc*" -and $Body -like "*$Description*"
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }

  Context 'Create New Maintenance Items Using Optional Parameters' {
    BeforeEach{
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Maintenance { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $Description = 'MyTestMaintenance'
      $StartUTC = '01/01/2000 10:00 am.'
      $EndUTC = '01/01/2000 11:00 am'
      $APIVersion = '99.9'
      $PingdomURL = 'https://api2.pingdom.com'
  
      $Params = @{
        AppKey      = $AppKey
        APIVersion  = $APIVersion
        Description = $Description
        PingdomURL  = $PingdomURL
        StartUTC    = $StartUTC
        EndUTC      = $EndUTC
      }
      New-PingdomMaintenance @Params
    }


    It 'Uses Post Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Method -eq 'Post'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $URI -eq "$PingdomURL/api/$APIVersion/maintenance"
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }
}