Import-Module '.\Pingdom.Management.Maintenance.psm1' -Force

Describe 'Get-PingdomMaintenance' {
  Context 'Get All Maintenance Items Using Default Options' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Maintenance { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      Get-PingdomMaintenance -AppKey $AppKey
    }
    
    It 'Uses Get Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Method -eq 'Get'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $URI -like "*/api/*/maintenance"
      }
    }

    It 'Has no body' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $null -eq $Body
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }

  Context 'Get All Maintenance Items Using Custom Options' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Maintenance { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $PingdomURL = 'https://api2.Pingdom.com'
      $APIVersion = '99.9'
      Get-PingdomMaintenance -AppKey $AppKey -API $APIVersion -PingdomURL $PingdomURL
    }


    It 'Uses Get Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Method -eq 'Get'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $URI -eq "$PingdomURL/api/$APIVersion/maintenance"
      }
    }

    It 'Has a valid body' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $null -eq $Body
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }  

  Context 'Maintenance Item By ID' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Maintenance { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $ID = '1234'
      Get-PingdomMaintenance -AppKey $AppKey -ID $ID
    }

    It 'Uses Get Method' {
      Assert-MockCalled Invoke-RestMethod -Exactly 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Method -eq 'Get'
      }
    }

    It 'Has a valid path' {
      Assert-MockCalled Invoke-RestMethod -Exactly 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $URI -like "*/api/*/maintenance/$ID"
      }
    }

    It 'Has a valid header' {
      Assert-MockCalled Invoke-RestMethod -Exactly 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }

    It 'Has a valid body' {
      Assert-MockCalled Invoke-RestMethod -Exactly 1 -ModuleName Pingdom.Management.Maintenance -ParameterFilter {
        $null -eq $Body
      }
    }
  }
}