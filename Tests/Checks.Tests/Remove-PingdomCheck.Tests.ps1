Import-Module '.\Pingdom.Management.Checks.psm1' -Force

Describe 'Remove-PingdomCheck' {
  Context 'Removes Check Using Default Options' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Checks { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $ID = '5678'
      Remove-PingdomCheck -AppKey $AppKey -ID $ID
    }

    It 'Uses Delete Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Method -eq 'Delete'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $URI -like "*/api/*/checks"
      }
    }

    It 'Contains Required Body Attributes' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Body.delcheckids -eq $ID
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }

  Context 'Remove Check Using Optional Parameters' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Checks { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $ID = '5678'
      $APIVersion = '99.9'
      $PingdomURL = 'https://api2.pingdom.com'

      $Params = @{
        AppKey     = $AppKey
        ID         = $ID
        APIVersion = $APIVersion
        PingdomURL = $PingdomURL
      }
      Remove-PingdomCheck @Params
    }
    

    It 'Uses Delete Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Method -eq 'Delete'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $URI -eq "$PingdomURL/api/$APIVersion/checks"
      }
    }

    It 'Contains Required Body Attributes' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Body.delcheckids -eq $ID
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }
}