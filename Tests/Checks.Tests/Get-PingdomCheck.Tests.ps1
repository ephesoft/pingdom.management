Import-Module '.\Pingdom.Management.Checks.psm1' -Force

Describe 'Get-PingdomCheck' {
  Context 'Get All Checks Using Default Options' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Checks { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      Get-PingdomCheck -AppKey $AppKey
    }    

    It 'Uses Get Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Method -eq 'Get'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $URI -like "*/api/*/checks"
      }
    }

    It 'Has a valid body' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        ($Body = @{
          include_tags = $true
        } | ConvertTo-Json -Compress) -eq $Body
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }

  Context 'Get All Checks Using Custom Options' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Checks { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $PingdomURL = 'https://api2.Pingdom.com'
      $APIVersion = '99.9'
      Get-PingdomCheck -AppKey $AppKey -API $APIVersion -PingdomURL $PingdomURL
    }

    It 'Uses Get Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Method -eq 'Get'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $URI -eq "$PingdomURL/api/$APIVersion/checks"
      }
    }

    It 'Has a valid body' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        ($Body = @{
          include_tags = $true
        } | ConvertTo-Json -Compress) -eq $Body
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }

  Context 'Get List of Checks Filtered By Tag' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Checks { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $TagFilter = '1234,5678'
      Get-PingdomCheck -AppKey $AppKey -TagFilter $TagFilter
    }

    It 'Uses Get Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Method -eq 'Get'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $URI -like "*/api/*/checks"
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }

    It 'Has a valid body' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        ($Body = @{
          include_tags = $true
          tags         = $TagFilter
        } | ConvertTo-Json -Compress) -eq $Body
      }
    }
  }

  Context 'Get Check By ID' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Checks { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $ID = '1234'
      Get-PingdomCheck -AppKey $AppKey -ID $ID
    }


    It 'Uses Get Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Method -eq 'Get'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $URI -like "*/api/*/checks/$ID"
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }

    It 'Has a valid body' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        ($Body = @{
          include_tags = $true
        } | ConvertTo-Json -Compress) -eq $Body
      }
    }
  }

  Context 'Use of Invalid Parameter Sets' {

    It 'Throws an Error' {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Checks { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $ID = '1234'
      $TagFilter = 'ABC'
      { Get-PingdomCheck -AppKey $AppKey -ID $ID -TagFilter $TagFilter } | Should -Throw
    }
  }
}