Import-Module '.\Pingdom.Management.Checks.psm1' -Force

Describe 'New-PingdomCheck' {
  Context 'Create New Check Using Default Options' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Checks { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $Name = 'Ima-Test-Check'
      $TargetHost = 'some.net'
      New-PingdomCheck -AppKey $AppKey -Name $Name -TargetHost $TargetHost
    }
    
    It 'Uses Post Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Method -eq 'Post'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $URI -like "*/api/*/checks"
      }
    }

    It 'Contains Required Body Attributes' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Body -like "*name=$Name*" -and $Body -like "*host=$TargetHost*" -and $Body -like "*type=*" 
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }

  Context 'Create New Check Using Optional Parameters' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Checks { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $Name = "TestCheck"
      $TargetHost = 'some.net'
      $Type = 'http'
      $APIVersion = '99.9'
      $IntegrationIDs = '12345,67890'
      $IPV6 = $true
      $NotifyAgainEvery = 1
      $NotifyWhenBackup = $true
      $Paused = $true
      $PingdomURL = 'https://api2.pingdom.com'
      $Port = 443
      $PostData = 'test'
      $ProbeFromRegion = 'NA'
      $Resolution = 1
      $ResponseTimeThreshold = 5000
      $SendNotificationWhenDown = 6
      $ShouldContain = 'true'
      $ShouldNotContain = 'false'
      $Tags = 'blue,green,orange'
      $TargetPath = '/health'
      $TeamIDs = '1234'
      $UserIDs = '1234,12345'

      $Params = @{
        AppKey                   = $AppKey
        Name                     = $Name
        TargetHost               = $TargetHost
        Type                     = $Type
        APIVersion               = $APIVersion
        IntegrationIDs           = $IntegrationIDs
        IPV6                     = $IPV6
        NotifyAgainEvery         = $NotifyAgainEvery
        PingdomURL               = $PingdomURL
        Paused                   = $Paused
        Port                     = $Port
        PostData                 = $PostData
        ProbeFilters             = $ProbeFromRegion
        Resolution               = $Resolution
        ResponseTimeThreshold    = $ResponseTimeThreshold
        SendNotificationWhenDown = $SendNotificationWhenDown
        ShouldContain            = $ShouldContain
        ShouldNotContain         = $ShouldNotContain
        Tags                     = $Tags
        TargetPath               = $TargetPath
        TeamIDs                  = $TeamIDs
        UserIDs                  = $UserIDs
      }
      New-PingdomCheck @Params
    }

    It 'Uses Post Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Method -eq 'Post'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $URI -eq "$PingdomURL/api/$APIVersion/checks"
      }
    }

    It 'Contains Required Body Attributes' {
      $Attributes = @(
        "encryption=$Encryption",
        "host=$TargetHost",
        "type=$Type",
        "name=$Name",
        "integrationids=$IntegrationIDs",
        "ipv6=$IPv6",
        "notifyagainevery=$NotifyAgainEvery",
        "notifywhenbackup=$NotifyWhenBackup",
        "paused=$Paused",
        "port=$Port",
        "postdata=$PostData"
        "probe_filters=region:$ProbeFromRegion",
        "resolution=$Resolution",
        "responsetime_threshold=$ResponseTimeThreshold",
        "sendnotificationwhendown=$SendNotificationWhenDown",
        "shouldcontain=$ShouldContain",
        "shouldnotcontain=$ShouldNotContain"
        "tags=$Tags",
        "url=$TargetPath",
        "teamids=$TeamIDs",
        "userids=$UserIDs")
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $null -ne ($Attributes | Where-Object { $Body -match $_ })
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }
}