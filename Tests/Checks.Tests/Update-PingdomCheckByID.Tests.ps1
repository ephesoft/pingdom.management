Import-Module '.\Pingdom.Management.Checks.psm1' -Force

Describe 'Update-PingdomCheckByID' {
  Context 'Update Check Using Default Options' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Checks { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $ID = '5678'
      Update-PingdomCheckByID -AppKey $AppKey -ID $ID
    }

    It 'Uses Put Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Method -eq 'Put'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $URI -like "*/api/*/checks/$ID"
      }
    }

    It 'Has No Body' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $null -eq $Body
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }

  Context 'Update Check Using Optional Parameters' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Checks { 
        $Response = [PSCustomObject] @{
          id = 5678
        }
        return $Response
      }
      $AppKey = '1234'
      $ID = '5678'
      $APIVersion = '99.9'
      $PingdomURL = 'https://api2.pingdom.com'
      $Params = @{
        AppKey     = $AppKey
        ID         = $ID
        APIVersion = $APIVersion
        PingdomURL = $PingdomURL
      }
      Update-PingdomCheckByID @Params
    }

    It 'Uses Put Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Method -eq 'Put'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $URI -eq "$PingdomURL/api/$APIVersion/checks/$ID"
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Checks -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }
}