Import-Module '.\Pingdom.Management.Probes.psm1' -Force

Describe 'Get-PingdomProbeList' {
  Context 'Get All Probes Using Default Options' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Probes { 
        $Response = [PSCustomObject] @{
          id = 1234
        }
        return $Response
      }
      $AppKey = '1234'
      Get-PingdomProbeList -AppKey $AppKey
    }    

    It 'Uses Get Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Probes -ParameterFilter {
        $Method -eq 'Get'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Probes -ParameterFilter {
        $URI -like "*/api/*/probes"
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Probes -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }

  Context 'Get All Probes Using Custom Options' {
    BeforeEach {
      Mock Invoke-RestMethod -ModuleName Pingdom.Management.Probes { 
        $Response = [PSCustomObject] @{
          id = 1234
        }
        return $Response
      }
      $AppKey = '1234'
      $PingdomURL = 'https://api2.Pingdom.com'
      $APIVersion = '99.9'
      Get-PingdomProbeList -AppKey $AppKey -API $APIVersion -PingdomURL $PingdomURL
    }


    It 'Uses Get Method' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Probes -ParameterFilter {
        $Method -eq 'Get'
      }
    }

    It 'Has a valid path' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Probes -ParameterFilter {
        $URI -eq "$PingdomURL/api/$APIVersion/probes"
      }
    }

    It 'Has a valid header' {
      Should -Invoke -CommandName Invoke-RestMethod -Times 1 -ModuleName Pingdom.Management.Probes -ParameterFilter {
        $Headers.Authorization -eq "Bearer $AppKey"
      }
    }
  }
}