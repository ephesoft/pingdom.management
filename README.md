# Pingdom.Management
This powershell module can be used to manage Pingdom checks, maintenance events and view uptime reports.

## How do I get set up?
* Make sure you have PowerShell 5.1 or greater installed
* Run `Install-Module Pingdom.Management`
* Run `Import-Module Pingdom.Management`
* Run `Get-Command -Module Pingdom.Management` to see available CMDlets
* See \build\test.ps1 for steps to run tests

## How To Get A Pingdom API Key
1. Login to Pingdom as an administrator
2. From the left navigation, click Integrations --> 'The Pingdom API'
3. At top-right, click 'Add API Token'
4. Name the key firstname.lastname-PowerShell

## Contributing
Please see the contributing.md file
