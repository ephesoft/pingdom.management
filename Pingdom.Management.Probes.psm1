<#
.Synopsis
  Returns the list of Pingdom uptime probes
.Description
  Returns the list of Pingdom uptime probes and their attributes
.Example
  Get-PingdomProbeList -AppKey $AppKey
  Returns the full list of Pingdom probes and their attributes
.Parameter AppKey
  The API key used to access the Pingdom API
.Parameter APIVersion
  Version of the Pingdom API to use
.Parameter PingdomURL
  The URL to pingdom's API
#>
Function Get-PingdomProbeList {
  Param (
    [Parameter (Mandatory = $true)]
    [string] $AppKey,
    [string] $PingdomURL = 'https://api.pingdom.com',
    [string] $APIVersion = '3.1'
  )
  $Method = 'Get'
  $Path = "/api/$APIVersion/probes"
  $headers = @{
    Authorization = "Bearer $AppKey"
  }
  (Invoke-RestMethod -Method $Method -Uri "$PingdomURL$Path" -Headers $headers).probes
}