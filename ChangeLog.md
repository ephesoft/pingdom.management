#ChangeLog
Version changes are logged here using semantic versioning (https://semver.org/)

## Deprecated features
*These will be removed in the next major release*
 - None

1.4.0 (2023-01-30)
----
 - Update Pester version to 5.4.0
 - Updated scope in test cases according to Pester 5 documentation
 - Replaces Assert-MockCalled with Should -Invoke as per Pester 5 documentation

1.3.0 (2021-08-31)
----
 - Module is now open source and published to PSGallery
 - Removed $IncludeTeams from Pingdom.Management.Maintenance.psm1 as ScriptAnalyzer reported this variable was not used

1.2.0 (2021-04-27)
----
 - Add CMDLet Get-PingdomCheckOutageCount
 - Add outage count to uptime report

1.1.0 (2020-09-11)
-----
 - Add CMDLet Get-PingdomCheckUptime

1.0.1
-----
 - Fixed bug in New-PingdomCheck where TargetPath wasn't being appended properly

1.0.0
-----
 - Initial Commit

- - - - -
Check the [Markdown Syntax Guide](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html) for basic syntax.