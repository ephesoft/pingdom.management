<#
.Synopsis
  Returns Pingdom Maintenance Events
.Description
  Returns a list of Pingdom maintenance events or a specific maintenance ID
.Example
  Get-PingdomMaintenance -AppKey $AppKey
  Returns the full list of Pingdom maintenance items
.Example
  Get-PingdomMaintenance -AppKey $AppKey -ID '123456'
  Returns the Pingdom maintenance item with ID 123456
.Example
  $MyChecks = '123456','789012','897654'
  $MyChecks | Get-PingdomMaintenance -AppKey $AppKey
  Returns an array of objects containing data for all maintenance items whose IDs were listed in $MyChecks
.Parameter AppKey
  The API key used to access the Pingdom API
.Parameter APIVersion
  Version of the Pingdom API to use
.Parameter ID
  If provided, filters results to the ID provided.  Can be passed in via pipeline and pipeline by property name.
.Parameter PingdomURL
  The URL to pingdom's API
#>
Function Get-PingdomMaintenance {
  [cmdletbinding()]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $AppKey,
    [Parameter (ValueFromPipelineByPropertyName = $true, ValueFromPipeline = $true)]
    [String] $ID,
    [string] $APIVersion = '3.1',
    [string] $PingdomURL = 'https://api.pingdom.com'
  )
  Begin {
    $BasePath = "/api/$APIVersion/maintenance"
    $Method = 'Get'
    $headers = @{
      Authorization = "Bearer $AppKey"
    }
  }
  Process {
    If ($ID) {
      $Path = "$BasePath/$ID"
    }
    Else {
      $Path = $BasePath
    }
    Return (Invoke-RestMethod -Method $Method -Uri "$PingdomURL$Path" -Headers $headers).maintenance
  }
}

<#
.Synopsis
  Adds an uptime check to an existing Pingdom maintenance event
.Description
  Adds an uptime check to an existing Pingdom maintenance event
.Example
  Add-CheckToPingdomMaintenance -AppKey $AppKey -MaintenanceID '123456' -CheckID '789012'
  Adds Pingdom uptime check with ID '789012' to the maintenance event '123456'
.Example
  $CheckIDs '123456','789012','98765'
  $CheckIDs | Add-CheckToPingdomMaintenance -Appkey $AppKey -MaintenanceID '123456'
  Adds all checks in $CheckIDs to the maintenance event '123456'
.Parameter AppKey
  The API key used to access the Pingdom API
.Parameter APIVersion
  Version of the Pingdom API to use
.Parameter CheckID
  Pingdom uptime check ID to be added to the maintenance event.  Can be passed via pipeline or pipline by propertyname.
.Parameter MaintenanceID
  If provided, filters results to the ID provided.  Can be passed in via pipeline and pipeline by property name.
.Parameter PingdomURL
  The URL to pingdom's API
#>
Function Add-CheckToPingdomMaintenance {
  [cmdletbinding(SupportsShouldProcess = $true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $AppKey,
    [Parameter (Mandatory = $true)]
    [String] $MaintenanceID,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true, ValueFromPipeline = $true)]
    [String] $CheckID,
    [string] $APIVersion = '3.1',
    [string] $PingdomURL = 'https://api.pingdom.com'
  )
  Begin {
    $Path = "/api/$APIVersion/maintenance/$MaintenanceID"
    $Method = 'Put'
    $headers = @{
    Authorization = "Bearer $AppKey"
    }

    # Get the existing uptime IDs associated with the maintenance event
    $MaintenanceItem = Get-PingdomMaintenance -AppKey $AppKey -ID $MaintenanceID
    [array]$CheckIDs = $MaintenanceItem.checks.uptime
  }
  Process {
    [array]$CheckIDs += $CheckID
  }
  End {

    # Add all of the check IDs to the body as a comma seperated string
    $Body = @{
      uptimeids = $CheckIDs -join (',')
    } | ConvertTo-Json

    # Call the Pingdom API and pass in the full list of IDs to associate with the maintenance event
    If ($PSCmdlet.ShouldProcess($MaintenanceItem.Description)) {
      Return Invoke-RestMethod -Method $Method -Uri "$PingdomURL$Path" -Headers $headers -Body $Body -ContentType 'application/json'
    }
  }
}

<#
.Synopsis
  Adds a maintenance event in Pingdom
.Description
  Adds a maintenance event to the account associated with the supplied appkey
.Example
  New-PingdomMaintenance -AppKey $AppKey -Description 'MyMaintenanceWindow' -StartUTC '1/1/2025 10:00 AM' -EndUTC '1/1/2025 11:00 AM' -UptimeCheckIDs '123456'
  Creates a new maintenance event for uptime check with ID 123456
.Example
  New-PingdomMaintenance -AppKey $AppKey -Description 'MyMaintenanceWindow' -RecurrenceType 'week' -RepeatEvery 1 -EffectiveTo '1/1/2016 '11:00 AM' -StartUTC '1/1/2025 10:00 AM' -EndUTC '1/1/2025 11:00 AM' -UptimeCheckIDs '123456'
  Creates a new maintenance event for uptime check with ID 123456 which recurrs every week from 10-11 AM UTC until 1/1/2016
.Parameter AppKey
  The API key used to access the Pingdom API
.Parameter APIVersion
  Version of the Pingdom API to use
.Parameter EndUTC
  The date and time to end the maintenance window in UTC time.  Format can be '1/1/2016 11:00 PM'.
.Parameter EffectiveTo
  Date and time when recurring maintenance events should end. Required for recurring maintenance events.
.Parameter PingdomURL
  The URL to pingdom's API
.Parameter RepeatEvery
  Specified whether recurrence intervals should be skipped (e.g. every 2 weeks).  Required for recurring maintenance events.
.Parameter RecurrenceType
  Default: None.  How frequently the maintenance window should recurr (day, week, month, none).
.Parameter StartUTC
  The date and time to start the maintenance window in UTC time.  Format can be '1/1/2016 11:00 PM'.
.Parameter TransactionCheckIDs
  A comma seperated string of transaction check IDs to associate with the maintenance window.
.Parameter UptimeCheckIDs
  A comma seperated string of uptime check IDs to associate with the maintenance window.
#>
Function New-PingdomMaintenance {
  [cmdletbinding(SupportsShouldProcess = $true, DefaultParameterSetName='Default')]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $AppKey,
    [Parameter (Mandatory = $true)]
    [string] $Description,
    [Parameter (Mandatory = $true)]
    [datetime] $StartUTC,
    [Parameter (Mandatory = $true)]
    [datetime] $EndUTC,
    [Parameter (ParameterSetName = 'Recurrence')]
    [ValidateSet ('day', 'week', 'month','none')]
    [String] $RecurrenceType = 'none',
    [Parameter (ParameterSetName = 'Recurrence')]
    [String] $RepeatEvery,
    [Parameter (ParameterSetName = 'Recurrence')]
    [DateTime] $EffectiveTo,
    [string] $APIVersion = '3.1',
    [string] $PingdomURL = 'https://api.pingdom.com',
    [array] $TransactionCheckIDs,
    [array] $UptimeCheckIDs
  )
  $QueryString = "/api/$APIVersion/maintenance"
  $Method = 'Post'
  $headers = @{
    Authorization = "Bearer $AppKey"
  }
  $StartEpoc = (New-TimeSpan -Start (Get-Date "01/01/1970") -End (Get-Date $StartUTC)).TotalSeconds
  $EndEpoc = (New-TimeSpan -Start (Get-Date "01/01/1970") -End (Get-Date $EndUTC)).TotalSeconds
  $Body = @{
    description = $Description
    from = $StartEpoc
    to = $EndEpoc
  }
  If ($UptimeCheckIDs) {
    $Body += @{
      uptimeids = $UptimeCheckIDs -join (',')
    }
  }
  If ($TransactionCheckIDs) {
    $Body += @{
      tmsids = $UptimeCheckIDs -join (',')
    }
  }
  If ($RecurrenceType -ne 'none') {
    $EffectiveToEPOC = (New-TimeSpan -Start (Get-Date "01/01/1970") -End (Get-Date $EffectiveTo)).TotalSeconds
    $Body += @{
      recurrencetype = $RecurrenceType
      repeatevery = $RepeatEvery
      effectiveto = $EffectiveToEPOC
    }
  }
  $Body = $Body | ConvertTo-Json
  If ($PSCmdlet.ShouldProcess($Description)) {
    Return (Invoke-RestMethod -Method $Method -Uri "$PingdomURL$QueryString" -Headers $headers -Body $Body -ContentType 'application/json').maintenance
  }
}

<#
.Synopsis
  Removes a Pingdom mainenance event
.Description
  Permanently deletes a Pingdom maintenance event
.Example
  Remove-PingdomMaintenance -AppKey $AppKey -ID '123456'
  Permanently deletes the Pingdom maintenance event with ID 123456
.Example
  $IDs | Remove-PingdomMaintenance -AppKey $AppKey
  Permanently deletes all maintenance IDs contained in $IDs.
.Parameter APIVersion
  Default: 3.1 Specifies the version of the API to use.
.Parameter AppKey
  The API key used to access the Pingdom API
.Parameter ID
  If provided, filters results to the ID provided.  Can be passed in via pipeline and pipeline by property name.
.Parameter PingdomURL
  The URL to pingdom's API
#>
Function Remove-PingdomMaintenance {
  [cmdletbinding(SupportsShouldProcess = $true)]
  Param (
    [Parameter (Mandatory = $true)]
    [string] $AppKey,
    [Parameter (Mandatory = $true, ValueFromPipelineByPropertyName = $true, ValueFromPipeline = $true)]
    [string] $ID,
    [string] $PingdomURL = 'https://api.pingdom.com',
    [string] $APIVersion = '3.1'
  )
  Begin {
    $Path = "/api/$APIVersion/maintenance"
    $Method = 'Delete'
    $headers = @{
      Authorization = "Bearer $AppKey"
    }
  }
  Process {
    [array]$MaintenanceIDs += $ID
  }
  End {
    $Body = @{
      maintenanceids = $MaintenanceIDs -join (',')
    }
    If ($PSCmdlet.ShouldProcess($ID)) {
      Return Invoke-RestMethod -Method $Method -Uri "$PingdomURL$Path" -Headers $headers -Body $Body
    }
  }
}