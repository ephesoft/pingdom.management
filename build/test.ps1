Install-Module PSScriptAnalyzer -Force
Install-Module Pester -RequiredVersion 5.4.0 -Force
Install-Module PowerShell-Yaml -Force
Import-Module PSScriptAnalyzer
Import-Module Pester
Import-Module PowerShell-Yaml
Invoke-Pester -EnableExit