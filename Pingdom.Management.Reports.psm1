Using module .\Pingdom.Management.Checks.psm1


<#
.Synopsis
  Returns an uptime report.
.Description
  Returns the uptime and number of outages for the time period and checks specified.
.Example
  Get-PingdomUptimeReport -AppKey $AppKey -From (Get-Date '01/01/2020') -To (Get-Date '01/01/2021') -Tags 'Production'
  Returns an uptime report for the year 2020 on all checks with the Production tag
.Parameter AppKey
  The API key used to access the Pingdom API
.Parameter From
  The PowerShell date/time to start the report from
.Parameter To
  The Powershell date/time to end the report
.Parameter IDs
  An array of check IDs to run the report on.  If provided, only the checks listed will be included in the report.
.Parameter Tags
  A comma separated string of tags.  If provided, only checks with the specified tags will be included in the report.
#>
Function Get-PingdomUptimeReport {
  Param (
    [Parameter (Mandatory = $true)]
    [string] $AppKey,
    [Parameter (Mandatory = $true)]
    [datetime] $From,
    [Parameter (Mandatory = $true)]
    [datetime] $To,
    [string] $Tags,
    [array] $IDs
  )

  $Params = @{
    AppKey = $AppKey
  }
  If ($Tags) {
    $Params += @{
      TagFilter = $Tags
    }
  }

  If ($IDs) {
    $Checks = $IDs | Get-PingdomCheck @Params -Verbose:$VerbosePreference
  }
  Else {
    $Checks = Get-PingdomCheck @Params -Verbose:$VerbosePreference
  }

  Foreach ($Check in $Checks) {
    Write-Progress -Activity "Calculating Uptime for check $($Check.Name)" -PercentComplete ($Checks.IndexOf($Check)/$Checks.Count*100)

    # Calculate uptime percentage for this check
    $uptimeData = Get-PingdomCheckUptime -AppKey $AppKey -From $From -To $To -ID $Check.Id
    $outageCount = Get-PingdomCheckOutageCount -AppKey $AppKey -From $From -To $To -ID $Check.Id
    $totalSeconds = $uptimeData.responseTime.To - $uptimeData.responseTime.From
    $upPercentage = 100-($uptimeData.status.totalDown/$totalSeconds*100)

    $ReportObj = [pscustomobject] @{
      Name = $Check.Name
      Hostname = $Check.Hostname
      id = $Check.id
      Uptime = [math]::Round($upPercentage,3)
      Outages = $outageCount
    }
    [array]$Report += $ReportObj
  }
  Return $Report
}
